using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace TodoList.Data
{
    public class WeatherForecastService
    {
        private static DataTable produits = new DataTable("produits");

        private static DataTable recettes = new DataTable("recettes");

        public Task<DataTable> GetProduitsAsync()
        {
            if (produits.Columns.Count == 0) {
                produits.Columns.Add("produit", typeof(string));
                produits.Columns.Add("prix", typeof(decimal));
                produits.PrimaryKey = new DataColumn[] { produits.Columns["produit"] };
                produits.Rows.Add("Café", 1);
                produits.Rows.Add("Sucre", 0.1);
                produits.Rows.Add("Crème", 0.5);
                produits.Rows.Add("Thé", 2);
                produits.Rows.Add("Eau", 0.2);
                produits.Rows.Add("Chocolat", 1);
                produits.Rows.Add("Lait", 0.4);

                recettes.Columns.Add("recette", typeof(string));
                for (int i = 0; i < produits.Rows.Count; i++)
                {
                    recettes.Columns.Add(produits.Rows[i]["produit"].ToString(), typeof(decimal));
                }
                recettes.PrimaryKey = new DataColumn[] { recettes.Columns["recette"] };
                recettes.Rows.Add("Coût", 0, 0, 0, 0, 0, 0, 0);
                foreach (DataRow row in produits.Rows)
                {
                    recettes.Rows[0][row["produit"].ToString()] = row["prix"];
                }
                recettes.Rows.Add("Expresso", 1, 0, 0, 0, 1, 0, 0);
                recettes.Rows.Add("Allongé", 1, 0, 0, 0, 2, 0, 0);
                recettes.Rows.Add("Capuccino", 1, 0, 1, 0, 1, 1, 0);
                recettes.Rows.Add("Chocolat", 0, 1, 0, 0, 1, 3, 2);
                recettes.Rows.Add("Thé", 0, 0, 0, 1, 2, 0, 0);
            }

            return Task.FromResult(recettes);
        }
    }
}
